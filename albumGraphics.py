import PyQt5
import argparse
import sys

class ImagesCollage()

def main():
    parser = argparse.ArgumentParser(prog="imcol")
    help_message = "read images' names from file and build with them a collage"
    parser.add_argument('-f', '--file', help=help_message)
    args = parser.parse_args()

    if args.file:
        pass
    else:
        sys.exit("File name should be entered")
