import itertools
import functools
import sys

image_names = [
    #"./images/icons/wait please.jpg",
    "IMG_20170523_213023.jpg",
    "IMG_20170621_202211.jpg",
    "IMG_20180202_172820.jpg",
    "IMG_20180206_204810.jpg",
    "IMG_20170523_213023.jpg",
    ]
home_dir = "/home/somnium/Images/mi5/part1/{}"
image_names = list(map(home_dir.format, image_names))
step = 10
amount = len(image_names)
left_corners = zip(*itertools.tee(range(0, amount * step, step)))
w, h = 248, 248
back = 255 * np.ones((w+(amount - 1)*step, h+(amount-1)*step, 3))
items = []

for image_name, corner in zip(image_names, left_corners):
    x, y = corner
    image = cv.imread(image_name, cv.IMREAD_COLOR)
    image = cv.resize(image, (w, h), interpolation=cv.INTER_LINEAR)
    iw, ih = image.shape[:2]
    back[x:x+iw, y:y+ih] = image

cv.imwrite('capture.jpg', back)