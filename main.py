import sys
from PyQt5 import QtWidgets
from source.gui.MainWindow import MainWindow

def main():
    app = QtWidgets.QApplication([])
    mw = MainWindow()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
