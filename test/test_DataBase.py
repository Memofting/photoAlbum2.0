#!/usr/bin/env python3
import shutil
import os
import unittest
import time

from concurrent.futures import ThreadPoolExecutor
from source.database.DataBase import DataBase
from source.database.ImageInfo import ImageInfo
from source.database.AlbumInfo import AlbumInfo


class TestDBAlbum(unittest.TestCase):
    image_names = tuple(map(lambda d: str(d) + '.png', range(1, 101)))

    one_element_database = {
        '1': AlbumInfo(database=None, name='1',
                        order=['1.png'],
                        images={'1.png': ImageInfo(name='1.png', albums_names=['1'])})
    }
    data_base = {
        '1': 
        AlbumInfo(database=None, name='1', order=['1.png', '2.png'],
                  images={
                      '1.png': ImageInfo(name='1.png', albums_names=['1', '2']),
                      '2.png': ImageInfo(name='2.png', albums_names=['1']),
                  }),
        '2': 
        AlbumInfo(database=None, name='2', order=['1.png'],
                  images={
                      '1.png': ImageInfo(name='1.png', albums_names=['1', '2'])
                  })
    }
    other_data_base = {
        '1': 
        AlbumInfo(database=None, name='1',
                  order=['3.png', '2.png', '4.png'],
                  images={
                      '3.png': ImageInfo(name='3.png', albums_names=['1']),
                      '2.png': ImageInfo(name='2.png', albums_names=['1', '3']),
                      '4.png': ImageInfo(name='4.png', albums_names=['1', '3']),
                  }),
        '3': 
        AlbumInfo(database=None, name='3',
                  order=['4.png', '2.png'],
                  images={
                      '4.png': ImageInfo(name='4.png', albums_names=['1', '3']),
                      '2.png': ImageInfo(name='2.png', albums_names=['1', '3']),
                  })
    }
    merge_result = {
        '1': 
        AlbumInfo(database=None, name='1',
                  order=['1.png', '3.png', '2.png', '4.png'],
                  images={
                      '1.png': ImageInfo(name='1.png', albums_names=['1', '2']),
                      '2.png': ImageInfo(name='2.png', albums_names=['1', '3']),
                      '3.png': ImageInfo(name='3.png', albums_names=['1']),
                      '4.png': ImageInfo(name='4.png', albums_names=['1', '3'])
                  }),
        '2': 
        AlbumInfo(database=None, name='2',
                  order=['1.png'],
                  images={
                      '1.png': ImageInfo(name='1.png', albums_names=['1', '2'])
                  }),
        '3': 
        AlbumInfo(database=None, name='3',
                  order=['4.png', '2.png'],
                  images={
                      '4.png': ImageInfo(name='4.png', albums_names=['1', '3']),
                      '2.png': ImageInfo(name='2.png', albums_names=['1', '3']),
                  })
    }
    concurrent_result = {
        '1': 
        AlbumInfo(database=None, name='1',
            order=list(image_names),
            images=dict(
                ((image_name, ImageInfo(name=image_name, albums_names=['1', '2'])) 
                for image_name in image_names))),
        '2': 
        AlbumInfo(database=None, name='2',
            order=list(image_names),
            images=dict(
                ((image_name, ImageInfo(name=image_name, albums_names=['1', '2'])) 
                for image_name in image_names)))
    }

    db_name = os.path.join(
        os.getcwd(), 'albums_databases',
        'test', 'LocalDataBase')
    new_db_name = os.path.join(
        os.getcwd(), 'albums_databases',
        'test', 'created')
    main_folder = os.path.join(
        os.getcwd(), 'albums_databases', 'test')

    def test_correct_create_album(self):
        try:
            DataBase(self.db_name)
        except Exception as exc:
            self.assertNotIsInstance(exc, ValueError,
                                     "Should create album, but raise "
                                     "ValueError instead. "
                                     "{}".format(str(exc)))

    def setUp(self):
        os.makedirs(self.main_folder, exist_ok=True)

    def tearDown(self):
        shutil.rmtree(
            self.main_folder, ignore_errors=True)

    def test_correct_open_album_with_full_name(self):
        try:
            db = DataBase(self.db_name)
            db.add_image('1.png', '1')
            full_name = db.full_name
            db = DataBase(full_name)
            self.assertEqual(db.get_albums_info(), self.one_element_database,
                             "Incorrect open album with sended full name.")
        except Exception as exc:
            self.assertNotIsInstance(
                exc, ValueError, "Should open existed database, but raise "
                "ValueError instead. {}".format(str(exc))
            )

    def test_open_existed_album(self):
        try:
            db = DataBase(self.db_name)
            full_name = db.full_name
            DataBase(full_name)
        except Exception as exc:
            self.assertNotIsInstance(exc, ValueError,
                                     "Should create album, "
                                     "but raise ValueError instead. "
                                     "{}".format(str(exc)))

    def test_emty_date_base_after_creation(self):
        db = DataBase(self.new_db_name)
        info = db.get_albums_info()
        self.assertEqual(len(info), 0,
                         "Not empty after creation: len "
                         "{}".format(str(len(info))))

    def test_correct_clear(self):
        db = DataBase(self.new_db_name)
        db.add_image('1.png', '1')
        db.clear()
        self.assertDictEqual(db.get_albums_info(), {},
                             "Incorrect clear. Albums "
                             "should have empty form but doesn't.")

    def test_right_order(self):
        db = DataBase(self.db_name)
        db.add_image('1.png', '1')
        db.add_image('2.png', '1')
        db.add_image('3.png', '1')
        db.add_image('4.png', '1')
        db.add_image('5.png', '1')
        order = db.get_albums_info()['1'].order
        self.assertEqual(order, ['1.png', '2.png', '3.png', '4.png', '5.png'])

    def test_image_add(self):
        db = DataBase(self.db_name)
        db.add_image('1.png', '1')
        db.add_image('2.png', '1')
        db.add_image('1.png', '2')
        self.assertDictEqual(db.get_albums_info(), self.data_base,
                             "Incorrect add. Albums "
                             "info has inapproproate form.")

    def test_image_add_twice(self):
        db = DataBase(self.db_name)
        db.add_image('1.png', '1')
        db.add_image('2.png', '1')
        db.add_image('1.png', '2')
        first_time = db.get_albums_info()['1'].images.get(
            '1.png').albums_names['1']
        time.sleep(0.5)
        db.add_image('1.png', '1')
        second_time = db.get_albums_info()['1'].images.get(
            '1.png').albums_names['1']
        self.assertLess(first_time, second_time,
                        "Incorrect add. After "
                        "second add time stayed the same.")

    def test_image_remove(self):
        db = DataBase(self.db_name)
        db.add_image('1.png', '1')
        db.add_image('2.png', '1')
        db.add_image('3.png', '1')
        db.add_image('1.png', '2')
        db.get_albums_info()
        db.remove_image('3.png', '1')
        self.assertDictEqual(db.get_albums_info(), self.data_base,
                             "Incorrect remove. "
                             "Albums info doesn't have approproate form.")

    def test_image_remove_twice(self):
        db = DataBase(self.db_name)
        db.add_image('1.png', '1')
        db.add_image('2.png', '1')
        db.add_image('3.png', '1')
        db.add_image('1.png', '2')
        db.get_albums_info()
        db.remove_image('3.png', '1')
        db.remove_image('3.png', '1')
        self.assertDictEqual(db.get_albums_info(), self.data_base,
                             "Incorrect twice remove. "
                             "Albums info doesn't have "
                             "appropriate form.")

    def test_image_remove_non_existed_image(self):
        db = DataBase(self.db_name)
        db.add_image('1.png', '1')
        db.add_image('2.png', '1')
        db.add_image('1.png', '2')
        db.get_albums_info()
        db.remove_image('3.png', '2')
        self.assertDictEqual(db.get_albums_info(), self.data_base,
                             "Incorrect remove. "
                             "Album change form "
                             "after non-existed image removing")

    def test_image_remove_from_non_existed_album(self):
        db = DataBase(self.db_name)
        db.remove_image('3.png', '1')
        self.assertDictEqual(db.get_albums_info(), {},
                             "Incorrect remove. Albums "
                             "change form after removing "
                             "from non-existed album.")

    def test_total_update_image_info(self):
        db = DataBase(self.db_name)
        image_bytes = b'here we are'
        ext = "PNG"
        db.add_image('1.png', '1', image_bytes=image_bytes, ext=ext)
        info = db.get_albums_info()['1'].images['1.png']
        self.assertEqual(info.image_bytes, image_bytes,
                         "Incorrect set the 'image_bytes'"
                         " in ImageInfo object")
        self.assertEqual(info.ext, ext,
                         "Incorrect set the 'ext' in "
                         "ImageInfo object")

    def test_clear_only_albums_entity(self):
        db = DataBase(self.db_name)
        db.add_image('1.png', '1')
        db.add_image('1.png', '2')
        db.add_image('2.png', '2')
        db.clear('2')
        info = db.get_albums_info()
        self.assertEqual(info, self.one_element_database,
                             "Incorrect clear behaviour. Alfter albums "
                             "history deleting shouldn't be entity "
                             "connected with this album")

    def test_merge(self):
        one_db = DataBase(self.db_name)
        one_db.add_image('1.png', '1')
        one_db.add_image('2.png', '1')
        one_db.add_image('1.png', '2')

        other_db = DataBase(self.db_name + '1')
        other_db.add_image('3.png', '1')
        other_db.add_image('2.png', '1')
        other_db.add_image('4.png', '1')
        other_db.add_image('4.png', '3')
        other_db.add_image('2.png', '3')

        one_db.merge(other_db)
        self.assertDictEqual(one_db.get_albums_info(), self.merge_result,
                             "Incorrect merge. Albums "
                             "have incorrect form after merging.")

    def test_concurrent(self):
        futures = []
        with ThreadPoolExecutor(max_workers=4) as executor:
            db = DataBase(self.db_name)
            for album in ['1', '2']:
                future = executor.submit(self.add_images, db, album, self.image_names) 
                futures.append(future)
        [future.result(500) for future in futures]
        self.assertDictEqual(db.get_albums_info(), self.concurrent_result,
                             "Concurrent hasn't passed. {}\r\n{}".format(
                                    db.get_albums_info(), self.concurrent_result))

    def add_images(self, db, album, images):
        for image in images:
            db.add_image(image, album)
        return 0
