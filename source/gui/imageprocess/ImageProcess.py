import itertools
import os
from functools import partial
from PyQt5 import QtCore, QtGui

from source.database.ImageGetter import EXTENTIONS
from source.database.DataBase import DataBase
from source.gui.imageprocess.Image import scale_image, get_bytes_and_ext


class ImageProcess(QtCore.QObject):
    finished = QtCore.pyqtSignal()
    downloaded = QtCore.pyqtSignal(str, QtGui.QImage)

    def __init__(self):
        super().__init__()
        self.is_stopped = False

    @QtCore.pyqtSlot()
    def stop(self):
        print("нас остановили")
        self.is_stopped = True

    @QtCore.pyqtSlot()
    def process(self):
        self.finished.emit()


class DownloadImages(ImageProcess):
    """
    Download all images from current scroll area's album. 
    For each image emits downloaded signal with image name 
    and image(QImage).
    """

    def __init__(self, scroll_area, *args, **kwargs):
        super().__init__()
        self.scroll_area = scroll_area
        self.size = (scroll_area.w, scroll_area.h)

    @QtCore.pyqtSlot()
    def process(self):
        info = self.scroll_area.album.get_info()
        all_image_names = filter(
            lambda img: img not in self.scroll_area.saved_images,
            info[DataBase.ORDER_NAME])
        for _ in range(self.scroll_area.amount_lines):
            image_names = itertools.islice(
                all_image_names,
                self.scroll_area.line_size)
            for image_name in image_names:
                if self.is_stopped:
                    self.finished.emit()
                    return
                image_info = info[DataBase.IMAGES_NAME][image_name]
                data = image_info.image_bytes
                ext = image_info.ext
                if data and ext:
                    image_bytes = QtCore.QByteArray(data)
                    image = QtGui.QImage()
                    image.loadFromData(image_bytes, ext)
                else:
                    image = QtGui.QImage(image_name)
                    image = scale_image(self.size, image)
                    bytes_and_ext = get_bytes_and_ext(image_name, image)
                    self.scroll_area.album.add_image(image_name, *bytes_and_ext)
                self.downloaded.emit(image_name, image)       
        self.finished.emit()


class UploadImages(ImageProcess):
    """
    Upload passed images to album, then emit signal downloaded.
    """

    def __init__(self, scroll_area, images, *args, **kwargs):
        super().__init__()
        self.scroll_area = scroll_area
        self.size = (scroll_area.w, scroll_area.h)
        print("amount images for upload: {}".format(len(images)))
        self.images = images

    @QtCore.pyqtSlot()
    def process(self):
        print("images: {}".format(self.images))
        all_image_names = filter(
            lambda img: img not in self.scroll_area.saved_images,
            self.images)
        print("filtered: {}".format(all_image_names))
        for _ in range(self.scroll_area.amount_lines):
            image_names = list(itertools.islice(
                all_image_names,
                self.scroll_area.line_size))
            print("Images for scalling and uploading.\n{}".format(image_names))
            scaled_images = list(map(
                partial(scale_image, self.size),
                map(QtGui.QImage, image_names)))
            print("Scaled images.\n{}".format(scaled_images))
            for image_name, image in zip(image_names, scaled_images):
                if self.is_stopped:
                    self.finished.emit()
                    return
                bytes_and_ext = get_bytes_and_ext(image_name, image)
                self.scroll_area.album.add_image(
                    image_name, *bytes_and_ext)
                self.downloaded.emit(image_name, image)
        self.finished.emit()

