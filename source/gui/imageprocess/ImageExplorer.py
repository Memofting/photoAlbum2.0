#!/usr/bin/env python3
from PyQt5 import QtCore, QtWidgets, QtGui
import sys
from source.database.ImageMetaData import ImageMetaData
from source.database.ImageInfo import ImageInfo
from source.gui.imageprocess.Image import scale_image


def empty_func(*args, **kargs):
    pass


class ScaledImageWidget(QtWidgets.QLabel):
    MIN_SIZE_PRODUCT = 10e2
    MAX_SIZE_PRODUCT = 10e6

    def __init__(self, image_name: str=None, init_pixmap: QtGui.QPixmap=None, parent=None):
        super().__init__(parent)

        self.setFrameStyle(QtWidgets.QFrame.Box |
                            QtWidgets.QFrame.Panel)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.setSizePolicy(
            QtWidgets.QSizePolicy.Minimum,
            QtWidgets.QSizePolicy.Minimum
        )
        self.initialized = False
        self.reinit(image_name, init_pixmap)


    def reinit(self, image_name, pixmap):
        if image_name and pixmap:
            self.initialized = True    
            self.setPixmap(pixmap)
            if isinstance(image_name, str):
                self.original_image = QtGui.QPixmap(image_name)
            self.aspect_ratio = (
                    self.original_image.width() /
                    self.original_image.height())


    def wheelEvent(self, a0: QtGui.QWheelEvent):
        if not self.initialized:
            return
        pixmap = self.pixmap()
        delta = a0.angleDelta().y() // 3
        width = pixmap.width() + delta
        height = width / self.aspect_ratio
        # добавить abs и убрать второе условие
        if (self.MIN_SIZE_PRODUCT < width*height <
            self.MAX_SIZE_PRODUCT and width + height > 0):
            pixmap = QtGui.QPixmap(
                self.original_image.scaled(
                    width, round(height),
                    transformMode=QtCore.Qt.SmoothTransformation))
            self.setPixmap(pixmap)


class ImageDescription(QtWidgets.QWidget):
    def __init__(self, image_name: str=None, parent=None):
        super().__init__(parent)
        self.description = QtWidgets.QLabel()
        self.description.setFrameStyle(QtWidgets.QFrame.Box |
                                       QtWidgets.QFrame.Panel)
        self.description.setAlignment(QtCore.Qt.AlignLeft |
                                      QtCore.Qt.AlignTop)
        self.description.setWordWrap(True)
        self.hbox = QtWidgets.QHBoxLayout()
        self.hbox.addWidget(self.description, 1)
        self.setLayout(self.hbox)

        self.reinit(image_name)

    def reinit(self, image_name):
        if image_name:
            if isinstance(image_name, ImageInfo):
                self.meta_data = image_name.meta_data
            else:
                self.meta_data = ImageMetaData(image_name)
            self.description.setText(str(self.meta_data))


class ImageExplorer(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.downloaded = False

        self.scroll_area_for_image = QtWidgets.QScrollArea(self)
        self.scroll_area_for_image.setVerticalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOn
        )
        self.scroll_area_for_image.setHorizontalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOn
        )
        self.scroll_area_for_image.wheelEvent = empty_func
        self.scroll_area_for_image.setWidgetResizable(True)
        self.scroll_area_for_image.setWidget(ScaledImageWidget())

        self.scroll_area_for_description = QtWidgets.QScrollArea(self)
        self.scroll_area_for_description.setWidgetResizable(True)
        self.scroll_area_for_description.setWidget(ImageDescription())

        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.main_layout.addWidget(self.scroll_area_for_image, stretch=2)
        self.main_layout.addWidget(self.scroll_area_for_description, stretch=1)

    @QtCore.pyqtSlot(str)
    def download_image(self, image_name: str):
        w = self.scroll_area_for_image.width()
        h = self.scroll_area_for_image.height()
        pixmap = QtGui.QPixmap.fromImage(QtGui.QImage(image_name))
        scaled_image = scale_image((w,h), pixmap)
        self.scroll_area_for_image.widget().reinit(image_name, scaled_image)
        image_info = self.parent.database.get_image_info_by_name(image_name)
        self.scroll_area_for_description.widget().reinit(image_info)

