import os
from PyQt5 import QtGui, QtCore

def scale_image(size: tuple, image):
    """
    scale image to fit in the size
    """
    w, h = size
    if (image.size().width() > w or image.size().height() > h):
        aspect_ratio_mode = QtCore.Qt.KeepAspectRatio
        transform_mode = QtCore.Qt.SmoothTransformation
        return image.scaled(w, h, aspectRatioMode=aspect_ratio_mode,
                            transformMode=transform_mode)
    return image    


def get_bytes_and_ext(image_name: str, image: QtGui.QImage):
    if image is None:
        return b'', ''
    pixmap = QtGui.QPixmap.fromImage(image)
    byte_buf = QtCore.QByteArray()
    buf = QtCore.QBuffer(byte_buf)
    buf.open(QtCore.QIODevice.WriteOnly)
    ext = os.path.splitext(image_name)[1][1:].upper()
    # in assumption that ext is valid extension
    try:
        pixmap.save(buf, ext)
        buf.close()
    finally:
        # don't have a handler for exceptions
        return byte_buf.data(), ext