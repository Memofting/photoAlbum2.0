from PyQt5 import QtWidgets

def iter_layout(layout):
    res = []
    return map(lambda i: layout.itemAt(i), range(layout.count()))