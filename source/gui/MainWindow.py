#!/usr/bin/env python3
import sys
import os
from functools import partial
from itertools import chain
from PyQt5 import QtCore, QtWidgets

from source.gui.menu.Menu import MainWindowWithMenu
from source.gui.dialogs.CheckOnValidName import CheckOnValidName
from source.gui.scrollarea.ScrollArea import ScrollArea
from source.gui.albums.Album import Album
from source.gui.albums.AlbumWidget import AlbumWidget
from source.gui.albums.AlbumExplorer import AlbumExplorer
from source.gui.imageprocess.ImageProcess import DownloadImages, UploadImages
from source.gui.imageprocess.ImageExplorer import ImageExplorer
from source.database.DataBase import DataBase

DEFAULT_DATABASE = "New database"
DEFAULT_ALBUM = "New album"

class MainWindow(MainWindowWithMenu):
    def __init__(self, database=None, parent=None):
        if database is None:
            database = DataBase(DEFAULT_DATABASE)
        self.database = database
        print("DATABASE: {}".format(self.database.full_name))
        super().__init__(database, parent)
        
        album = Album(DEFAULT_ALBUM, self.database)
        self.select_album.emit(DEFAULT_ALBUM)

        # layout = QtWidgets.QGridLayout()
        splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        image_explorer = ImageExplorer(self)
        scroll_area = ScrollArea(album=album, parent=self)
        album_explorer = AlbumExplorer(parent=self)

        splitter.addWidget(image_explorer)
        splitter.addWidget(scroll_area)
        splitter.addWidget(album_explorer)
        
        scroll_area.select_album.connect(self.album_menu.set_checked)
        scroll_area.select_album.connect(album_explorer.set_checked)
        scroll_area.explore_image.connect(image_explorer.download_image)
        scroll_area.reset_albums.connect(self.show_albums)
        scroll_area.start_db_connection_thread(DownloadImages)
        album_explorer.show_album.connect(self.change_album)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(splitter)
        window = QtWidgets.QWidget()
        window.setLayout(layout)
        self.setCentralWidget(window)
        self.album_menu.clear_cur_album.connect(self.clear_album)
        self.album_menu.add_album_to_explorer.connect(self.show_albums_plus_other)
        self.album_menu.upload_geogroups.connect(scroll_area.upload_geogroups)

        self.database_menu.clear_window.connect(scroll_area.clear_area)
        self.database_menu.clear_window.connect(self.show_albums)
        self.database_menu.change_album.connect(self.show_albums)
        self.database_menu.change_album.connect(self.change_album)

        self.open_images_event.connect(self.upload_images)
        self.add_images_event.connect(self.add_images)

        self.image_explorer = image_explorer 
        self.scroll_area = scroll_area
        self.album_explorer = album_explorer

        self.resize(QtCore.QSize(720, 480))
        self.show_albums()
        self.showNormal()

    @QtCore.pyqtSlot(list)
    def show_albums_plus_other(self, names):
        self.show_albums(names)

    @QtCore.pyqtSlot()
    def show_albums(self, additional_names: list=None):
        if additional_names is None:
            additional_names = []
        names = list(chain(self.database.get_albums_info().keys(), 
                           additional_names))
        self.album_explorer.reset_albums(names)
        self.set_albums.emit(names)

    @QtCore.pyqtSlot(str)
    def change_album(self, new_album_name):
        self.scroll_area.change_album(Album(new_album_name, self.database))
    
    @QtCore.pyqtSlot(list)
    def upload_images(self, images):
        self.clear_album(is_change_album=False)
        self.scroll_area.upload_images(images)

    @QtCore.pyqtSlot(list)
    def add_images(self, images):
        self.scroll_area.upload_images(images)

    @QtCore.pyqtSlot()
    def clear_album(self, is_change_album=True):
        # для только что созданных альбомов, в которые мы 
        # еще ничего не добаивили, мы удаляем их (заблокируй опции open)
        self.scroll_area.clear_area()
        self.scroll_area.album.clear()
        self.show_albums()
        
        if is_change_album and self.album_menu.actions:
            self.change_album(self.album_menu.actions[0].text())

    def closeEvent(self, ev):
        self.scroll_area.stop_download_thread()
        self.album_explorer.stop_threads()
        ev.accept()