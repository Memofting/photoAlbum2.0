from PyQt5 import QtWidgets, QtCore, QtGui
import itertools
import time
from functools import wraps, partial
from threading import Lock
import os
from sklearn.cluster import KMeans
import numpy as np

from source.gui.albums.Album import Album
from source.gui.scrollarea.ScrollLabel import ScrollLabel
from source.gui.imageprocess.ImageProcess import DownloadImages, UploadImages, ImageProcess
from source.gui.imageprocess.Image import scale_image

from source.database.ImageGetter import EXTENTIONS
from source.database.AlbumInfo import AlbumInfo
import source.database.ImageGetter as ImageGetter


class ScrollArea(QtWidgets.QScrollArea):
    """
    ScrollArea is class, that contain central widget with images,
    provided methods to interract with them.
    to add: change albums
    """
    explore_image = QtCore.pyqtSignal(str, QtGui.QPixmap)
    select_album = QtCore.pyqtSignal(str)
    add_album = QtCore.pyqtSignal(AlbumInfo)
    reset_albums = QtCore.pyqtSignal()

    def __init__(self, album: Album, parent, line_size=4,
                 amount_lines=30, img_size=(124, 124)):
        super().__init__(parent)
        self.album = album
        self.parent = parent
        self.saved_images = set()
        self.scroll_labels = []

        self.selected_image = None
        self.w, self.h = img_size
        self.line_size = line_size
        self.amount_lines = amount_lines
        self.cur_row, self.cur_col = 0, 0

        self.window = QtWidgets.QWidget()
        self.window.setLayout(QtWidgets.QGridLayout())
        self.setWidget(self.window)
        self.setWidgetResizable(True)

        self.locker = Lock()
        self.alive_workers = set()
        self.alive_threads = set()

    @QtCore.pyqtSlot(str, QtGui.QImage)
    def create_label(self, name, image):
        new_label = ScrollLabel(image=image, image_name=name, 
                                w=self.w, h=self.h, parent=self)
        new_label.clicked.connect(self.double_click_on_image)
        self.saved_images.add(name)
        self.scroll_labels.append(new_label)
        self.show_last_label()

    def show_last_label(self):
        if not self.scroll_labels:
            raise ValueError("Try to show empty image")
        self.window.layout().addWidget(self.scroll_labels[-1], 
                              self.cur_row,
                              self.cur_col,
                              alignment=QtCore.Qt.AlignCenter)
        self.cur_col += 1
        if self.cur_col >= self.line_size:
            self.cur_row += 1
            self.cur_col = 0
    
    @QtCore.pyqtSlot(QtWidgets.QWidget)
    def double_click_on_image(self, image_widget):
        self.explore_image.emit(image_widget.name, image_widget.label.pixmap())
        if self.selected_image is not None:
            self.selected_image.set_is_clicked(False)
            self.selected_image.leaveEvent(None)
        self.selected_image = image_widget

    def change_album(self, new_album: Album):
        self.clear_area()
        is_changed = True
        with self.locker:
            for thread in self.alive_threads:
                # after stopping we sure that threads 
                # no more change the state of scroll area
                thread.finished.connect(partial(self._change_album, new_album))
                break
            else:
                is_changed = False
        if not is_changed:
            self._change_album(new_album)

    @QtCore.pyqtSlot(Album)
    def _change_album(self, new_album: Album):
        self.album = new_album
        self.clear_area()
        self.start_db_connection_thread(DownloadImages)

    def clear_area(self):
        self.selected_image = None
        self.stop_download_thread()
        indeces = zip(
            itertools.chain.from_iterable(
                [i] * self.line_size for i in range(self.cur_row)),
            itertools.cycle(range(self.line_size)))
        self.cur_col, self.cur_row = 0, 0
        for x,y in indeces:
            item = self.window.layout().itemAtPosition(x, y)
            self.window.layout().removeItem(item)
        list(map(lambda label: label.deleteLater(), self.scroll_labels))
        self.scroll_labels = []
        self.saved_images = set()

    def upload_images(self, image_names):
        self.start_db_connection_thread(UploadImages, images=image_names)

    @staticmethod
    def name_from_geo_tag(tag):
        return "GEOTAG: {:.3};{:.3}".format(*tag)


    def get_geogroups(self, n_clusters):
        """
        Build centers with scipy kmeans algorithm by saved images' geotags
        return: tuple(album_name, image_name)
                where album_name is computed center for image groups.
        if we haven't images then raised StopIteration, elsewhere sending None, n_clusters
        and get amount images
        """
        if not self.saved_images:
            return
        images_info = list(map(self.parent.database.get_image_info_by_name,
                                self.saved_images))
        # это место можно делегировать (т.е посылать данные и получать их)
        geo_tags = np.array(
            list(filter(lambda ll: ll[0] != None and ll[1] != None, 
                        map(lambda im_info: im_info.get_geo_tags(), images_info))), 
            dtype='float64')
        if geo_tags is None:
            return
            
        kmeans = KMeans(n_clusters=6, random_state=0).fit(geo_tags)
        centers = kmeans.cluster_centers_
        for image_info in images_info:
            tags = image_info.get_geo_tags()
            if tags[0] != None and tags[1] != None:
                center = centers[kmeans.predict(np.array([tags]))[0]]
                yield self.name_from_geo_tag(center), image_info.name

    @QtCore.pyqtSlot()
    def upload_geogroups(self, n_clucters: int=4):
        # придумай блокировку на время загрузки (пробовал с окнами, но они
        # не рендерились, пока не загрузятся все картинки в альбом)
        for album_name, image_name in self.get_geogroups(n_clucters):
            self.parent.database.add_image(image_name=image_name, albums_name=album_name)    
        self.reset_albums.emit()

    @QtCore.pyqtSlot(ImageProcess)
    def start_db_connection_thread(self, Worker: ImageProcess, 
                                   handler=None, 
                                   *args, **kwargs):
        self.select_album.emit(self.album.name)
        thread = QtCore.QThread()
        worker = Worker(self, *args, **kwargs)
        self.add_worker_thread(worker, thread)
        worker.moveToThread(thread)
        # error handler there -> ...
        #  почему нельзя вот так вызывать
        #    thread.started.connect(partial(self.add_worker_thread, worker, thread)) 
        # (потому что переменные worker и thread берутся из локальной области видимости,
        #  а по выходу из тела функции все локальные объекты уничтожаются (partial не 
        #  сохраняет объекты))
        thread.started.connect(worker.process)
        
        worker.finished.connect(thread.quit)
        # floating assignment баааааг
        if handler is None:
            worker.downloaded.connect(self.create_label)
        
        # здесь ссылки на объекты сохранились в полях объекта, поэтому мы можем 
        # к ним обращаться даже после выхода из метода
        thread.finished.connect(partial(self.discard_worker_thread, worker, thread))
        thread.finished.connect(worker.deleteLater)
        thread.finished.connect(thread.deleteLater)
        thread.start()


    @QtCore.pyqtSlot()
    def add_worker_thread(self, worker, thread):
        with self.locker:
            self.alive_threads.add(thread)
            self.alive_workers.add(worker)


    @QtCore.pyqtSlot()
    def discard_worker_thread(self, worker, thread):
        with self.locker:
            self.alive_threads.discard(thread)
            self.alive_workers.discard(worker)


    def stop_download_thread(self):
        with self.locker:
            for worker in self.alive_workers:
                try:
                    worker.downloaded.disconnect(self.create_label)
                except Exception:
                    pass
                finally:
                    worker.stop()
        