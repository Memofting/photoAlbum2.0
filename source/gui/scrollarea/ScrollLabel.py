#!/usr/bin/env python3
from PyQt5 import QtCore, QtWidgets, QtGui
import os
import sys
from source.gui.graphics.OnEnterChangeColorWidget import OnEnterChangeColorWidget
from source.gui.graphics.BackShadow import BackShadow


class ScrollLabel(OnEnterChangeColorWidget, QtWidgets.QWidget):
    """Класс, отвечающий функционалу фотографии на scroll_area. По клику
    посылает сигнал clicked[Qlabel], который содержит иформацию о фотографии,
    которая была выбрана. При наведении курсора на область генерируется
    сигнал entered, при уводе указателя мыши с области left.
    """

    clicked = QtCore.pyqtSignal(QtWidgets.QWidget)

    def __init__(self, image, parent,
                 image_name="unknown",
                 w=120, h=120):
        super().__init__(parent)
        self.name = image_name
        self.label = QtWidgets.QLabel()
        self.label.setPixmap(QtGui.QPixmap.fromImage(image))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        # настройка теней
        self.label.setGraphicsEffect(BackShadow())
        
        self.image_name_label = QtWidgets.QLabel(
            self.get_short_name_from_full(image_name))
        self.image_name_label.setAlignment(QtCore.Qt.AlignCenter)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.label)
        vbox.addWidget(self.image_name_label)
        # self.setSizePolicy(QtWidgets.QSizePolicy.Ignored, 
        #                    QtWidgets.QSizePolicy.Ignored)
        self.setLayout(vbox)
        # self.resize(w, h)

    @staticmethod
    def get_short_name_from_full(image_name, defualt_suffix="...", width=10):
        name = os.path.split(image_name)[-1]
        if len(name) > width:
            name = name[:width - len(defualt_suffix)] + defualt_suffix
        return name


    def mouseDoubleClickEvent(self, event):
        self.set_permanent_color()
        self.is_clicked = True
        self.clicked.emit(self)
