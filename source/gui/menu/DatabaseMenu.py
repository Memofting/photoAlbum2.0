from shutil import copyfile
from PyQt5 import QtWidgets, QtCore

from source.database.DataBase import DataBase
from source.gui.dialogs.CheckOnValidName import CheckOnValidName
from source.gui.dialogs.AddDatabaseDialog import AddDatabaseDialog
from source.gui.dialogs.SaveDatabaseDialog import SaveDatabaseDialog
from source.gui.menu.FileMenu import PrintArgs
from source.gui import MainWindow


class DatabaseMenu(QtWidgets.QMenu):
    add_database_event = QtCore.pyqtSignal(str)
    change_album = QtCore.pyqtSignal(str)
    clear_window = QtCore.pyqtSignal()

    def __init__(self, database, parent=None):
        super().__init__(parent)
        self.database = database
        self.setTitle("&Database")

        self.addAction(self.get_database_related_act(
            "Create", self.create_database))
        self.addAction(self.get_database_related_act(
            "Add", self.add_database))
        self.addAction(self.get_database_related_act(
            "Save", self.save_database))
        self.addAction(self.get_database_related_act(
            "Clear", self.clear_database))

    def get_database_related_act(self, name, handler):
        act = QtWidgets.QAction(name, self)
        act.triggered.connect(handler)
        return act

    @QtCore.pyqtSlot()
    @PrintArgs("create database")
    def create_database(self):
        err_message = "Failed with creation database"
        dialog = CheckOnValidName(
            self, "Create database", "Enter new database's name", text="New database")
        dialog.success.connect(lambda _: self.clear_database())
        dialog.success.connect(lambda name: self.database.merge(DataBase(name)))
        dialog.success.connect(lambda name: self.change_album.emit(MainWindow.DEFAULT_ALBUM))
        dialog.failed.connect(lambda name: print(err_message))
        dialog.exec_()
    
    @QtCore.pyqtSlot()
    @PrintArgs("add database")
    def add_database(self):
        dialog = AddDatabaseDialog(self)
        err_message = "Failed with adding database"
        dialog.success.connect(lambda name: self.database.merge(DataBase(name)))
        dialog.success.connect(lambda name: self.change_album.emit(MainWindow.DEFAULT_ALBUM))
        dialog.failed.connect(lambda: print(err_message))
        dialog.exec_()
        
    
    @QtCore.pyqtSlot()
    @PrintArgs("save database")
    def save_database(self):
        dialog = SaveDatabaseDialog(self)
        err_message = "Failed with saving database"
        dialog.success.connect(lambda name:  copyfile(self.database.full_name, name))
        dialog.failed.connect(lambda: print(err_message))
        dialog.exec_()
    
    @QtCore.pyqtSlot()
    @PrintArgs("clear database")
    def clear_database(self):
        self.database.clear()
        self.clear_window.emit()