from PyQt5 import QtWidgets, QtCore
from source.gui.dialogs.CheckOnValidName import CheckOnValidName

class AlbumMenu(QtWidgets.QMenu):
    selected = QtCore.pyqtSignal(str)
    rejected = QtCore.pyqtSignal(str)
    add_album_to_explorer = QtCore.pyqtSignal(list)
    clear_cur_album = QtCore.pyqtSignal()
    add_new_album = QtCore.pyqtSignal()
    upload_geogroups = QtCore.pyqtSignal()
    

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setTitle("&Albums")
        self.actions = []
        self.actGroup = QtWidgets.QActionGroup(self)
        self.actGroup.triggered.connect(self.album_selected)
        self.set_albums([])

        self.addAction(self.get_album_related_act(
            "Clear", lambda act: self.clear_cur_album.emit()))
        self.addAction(self.get_album_related_act(
            "Add", lambda act: self.add_new_album.emit()))
        self.addAction(self.get_album_related_act(
            "Get geogroups", lambda act: self.upload_geogroups.emit()))
        self.add_new_album.connect(self.add_album)

    @QtCore.pyqtSlot(list)
    def set_albums(self, albums: list):
        """
        albums - list of album names
        """
        self.clear_group()
        for album in albums:
            act = self.get_group_act(album)
            self.actions.append(act)
            self.addAction(act)

    @QtCore.pyqtSlot(str)
    def set_checked(self, name):
        albums = dict(zip(map(lambda a: a.text(), self.actions), self.actions))
        if name in albums.keys():
            albums[name].setChecked(True)
    
    def clear_group(self):
        for act in self.actions:
            self.actGroup.removeAction(act)
            act.deleteLater()
        self.actions = []

    def get_group_act(self, name: str):
        act = QtWidgets.QAction(name, self.actGroup)
        act.setCheckable(True)
        act.setIconVisibleInMenu(False)
        return act

    def get_album_related_act(self, name, handler):
        act = QtWidgets.QAction(name, self)
        act.triggered.connect(handler)
        return act


    @QtCore.pyqtSlot(QtWidgets.QAction)
    def album_selected(self, act):
        self.selected.emit(act.text())

    
    @QtCore.pyqtSlot()
    def add_album(self):
        dialog = CheckOnValidName(
            self, "Add album", "Enter new album's name", text="New album")
        dialog.success.connect(lambda name: self.add_album_to_explorer.emit([name]))
        dialog.success.connect(lambda name: self.selected.emit(name))
        dialog.failed.connect(
            lambda name: print("Invalid name: {}".format(name)))
        dialog.exec_()