import os
from PyQt5 import QtWidgets, QtCore


class SaveMessageBox(QtWidgets.QWidget):
    """
    Message box, который появляется при желании пользователя сохранить
    результаты работы в отдельный файл с базой данных.
    """
    save_event = QtCore.pyqtSignal()
    path_for_saving_event = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.save_event.connect(self.save_dialog)

    @QtCore.pyqtSlot()
    def save_query(self):
        reply = QtWidgets.QMessageBox.question(self, "Save",
                                               "Are you sure to save result?",
                                               QtWidgets.QMessageBox.Yes |
                                               QtWidgets.QMessageBox.No,
                                               QtWidgets.QMessageBox.No)
        if reply == QtWidgets.QMessageBox.Yes:
            self.save_event.emit()

    @QtCore.pyqtSlot()
    def save_dialog(self):
        file_name = QtWidgets.QFileDialog.getSaveFileName(
            self, "Save file", '')
        if file_name[0]:
            self.path_for_saving_event.emit(
                os.path.normpath(file_name[0]))
