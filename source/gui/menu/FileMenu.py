import os
from functools import partial, wraps
from PyQt5 import QtWidgets, QtGui, QtCore
from source.database.ImageGetter import EXTENTIONS


class FileAction(QtWidgets.QAction):
    def __init__(self, parent, icon_path, name, 
                 short_cut, status_tip, handler):
        super().__init__(QtGui.QIcon(icon_path), name, parent)
        self.setShortcut(short_cut)
        self.setStatusTip(status_tip)
        self.triggered.connect(handler)

class FileMenu(QtWidgets.QMenu):
    open_dir_event = QtCore.pyqtSignal(str)
    add_dir_event = QtCore.pyqtSignal(str)
    open_files_event = QtCore.pyqtSignal(list)
    add_files_event = QtCore.pyqtSignal(list)
    get_icon_path = partial(os.path.join, os.getcwd(), "image", "icons")

    def __init__(self, parent):
        super().__init__(parent)
        open_dir_icon = self.get_icon_path("open_folder.png")
        add_dir_icon = self.get_icon_path("add_folder.png")
        open_file_icon = self.get_icon_path("open_file.png")
        add_file_icon = self.get_icon_path("add_file.png")
        exit_icon = self.get_icon_path("exit.ico")

        self.open_dir_action = FileAction(self, open_dir_icon, "Open folder",
                                       "Ctrl+O", "Open folder as new album",
                                       self.show_open_dir_dialog)
        self.add_dir_action = FileAction(self, add_dir_icon, "Add folder",
                                         "Ctrl+A", "Add folder to album.",
                                         self.show_add_dir_dialog)
        self.open_files_action = FileAction(self, open_file_icon, 
                                            "Open files", "Ctrl+Shift+O",
                                            "Open files as new album.",
                                            self.show_open_files_dialog)
        self.add_files_action = FileAction(self, add_file_icon, "Add files",
                                           "Ctrl+Shift+A", 
                                           "Add files to album.",
                                            self.show_add_files_dialog)
        self.exit_action = QtWidgets.QAction(QtGui.QIcon(exit_icon), 
                                             "&Exit", self)
        self.exit_action.setShortcut("Ctrl+Q")
        self.exit_action.triggered.connect(QtWidgets.qApp.exit)

        self.setTitle("&File")
        self.addAction(self.open_dir_action)
        self.addAction(self.add_dir_action)
        self.addAction(self.open_files_action)
        self.addAction(self.add_files_action)
        self.addAction(self.exit_action)

        self.image_filter = "Images ({})".format(
            " ".join(map(lambda e: "*"+e, EXTENTIONS)))

    @staticmethod
    def translate_to_norm_view(paths):
        return list(map(os.path.normpath, paths))

    @staticmethod
    def show_dialog(dialog: QtWidgets.QFileDialog, event: QtCore.pyqtSignal):
        if dialog.exec_():
            file_names = FileMenu.translate_to_norm_view(
                dialog.selectedFiles())
            event.emit(file_names)

    def show_open_dir_dialog(self):
        folder = QtWidgets.QFileDialog.getExistingDirectory(
            parent=self, caption="open folder", directory="~")
        if folder:
            self.open_dir_event.emit(folder)

    def show_add_dir_dialog(self):
        folder = QtWidgets.QFileDialog.getExistingDirectory(
            parent=self, caption="add folder", directory="~")
        if folder:
            self.add_dir_event.emit(folder)

    def show_add_files_dialog(self):
        arr = QtWidgets.QFileDialog.getOpenFileNames(
            parent=self, caption="add files", directory="~",
            filter=self.image_filter, initialFilter=self.image_filter)
        if arr[0]:
            self.add_files_event.emit(arr[0])

    def show_open_files_dialog(self):
        arr = QtWidgets.QFileDialog.getOpenFileNames(
            parent=self, caption="open files", directory="~",
            filter=self.image_filter, initialFilter=self.image_filter)
        if arr[0]:
            self.open_files_event.emit(arr[0])

class PrintArgs():
    def __init__(self, name):
        super().__init__()
        self.name = name

    def __call__(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            print("{}:[{}],({})".format(self.name, args, kwargs))
            return func(*args, **kwargs)
        return wrapper