import re
import os
import sys
from functools import partial, wraps
from PyQt5 import QtWidgets, QtCore, QtGui

from source.database.ImageGetter import get_files_from
from source.database.DataBase import DataBase
from source.gui.menu.FileMenu import FileAction, FileMenu, PrintArgs
from source.gui.menu.AlbumMenu import AlbumMenu
from source.gui.menu.DatabaseMenu import DatabaseMenu
from source.gui.dialogs.CheckOnValidName import CheckOnValidName

class MainWindowWithMenu(QtWidgets.QMainWindow):
    
    set_albums = QtCore.pyqtSignal(list)
    select_album = QtCore.pyqtSignal(str)
    open_images_event = QtCore.pyqtSignal(list)
    add_images_event = QtCore.pyqtSignal(list)

    


    def __init__(self, database, parent=None):
        super().__init__(parent)

        menu_bar = self.menuBar()
        file_menu = FileMenu(self)
        file_menu.open_dir_event.connect(self.open_dir)
        file_menu.add_dir_event.connect(self.add_dir)
        file_menu.open_files_event.connect(self.open_files)
        file_menu.add_files_event.connect(self.add_files)
        menu_bar.addMenu(file_menu)

        album_menu = AlbumMenu(self)
        self.set_albums.connect(album_menu.set_albums)
        self.select_album.connect(album_menu.set_checked)
        album_menu.selected.connect(self.change_album)
        self.album_menu = album_menu
        menu_bar.addMenu(album_menu)

        database_menu = DatabaseMenu(database, self)
        self.database_menu = database_menu
        menu_bar.addMenu(database_menu)
        

    @QtCore.pyqtSlot(str)
    @PrintArgs("open folder")
    def open_dir(self, folder_name):
        self.open_images_event.emit(list(get_files_from(folder_name)))

    @QtCore.pyqtSlot(str)
    @PrintArgs("add folder")
    def add_dir(self, folder_name):
        self.add_images_event.emit(list(get_files_from(folder_name)))

    @QtCore.pyqtSlot(list)
    @PrintArgs("open files")
    def open_files(self, images):
        self.open_images_event.emit(images)

    @QtCore.pyqtSlot(list)
    @PrintArgs("add files")
    def add_files(self, images):
        self.add_images_event.emit(images)

    @QtCore.pyqtSlot(str)
    @PrintArgs("change album")
    def change_album(self, album):
        pass
    