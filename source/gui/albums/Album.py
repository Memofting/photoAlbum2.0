from PyQt5 import QtCore, QtGui
from source.database.DataBase import DataBase
from source.database.AlbumInfo import AlbumInfo

import os

class Album:
    ICON_PATH = os.path.join(".", "images", "icons", "album.png")
    def __init__(self, name, database: DataBase):
        """
        При инициализации мы можем либо загрузить уже имеющийся альбом, либо
        создать новый.
        """
        print("Init album: {},{}".format(name, database.full_name))
        self.name = name
        self._database = database

    def get_info(self):
        whole_db_data = self._database.get_albums_info().get(
            self.name, AlbumInfo(database=self._database, name=self.name,
                                 order=list(),
                                 images=dict()))
        return {
            DataBase.ORDER_NAME: whole_db_data.order,
            DataBase.IMAGES_NAME: whole_db_data.images
        }

    def get_preview(self):
        """
        Build preview image for album with images in album.
        """
        print("database: {}".format(self._database.full_name))
        album_info = self._database.get_albums_info().get(self.name)
        print("Find album info: {}".format(album_info))
        if album_info:
            return album_info.get_preview()
        return QtGui.QPixmap(self.ICON_PATH)

            

    @QtCore.pyqtSlot(str)
    def add_image(self, full_image_name, image_bytes: bytes, ext="JPG"):
        self._database.add_image(
            image_name=full_image_name,
            albums_name=self.name,
            image_bytes=image_bytes,
            ext=ext
        )

    @QtCore.pyqtSlot(str)
    def remove_image(self, full_image_name):
        self._database.remove_image(
            image_name=full_image_name,
            albums_name=self.name)

    @QtCore.pyqtSlot()
    def clear(self):
        self._database.clear(albums_name=self.name)

    @QtCore.pyqtSlot()
    def merge(self, other_albums_db: DataBase):
        self._database.merge(other_albums_db)