from PyQt5 import QtWidgets, QtCore
from source.gui.albums.AlbumWidget import AlbumWidget
from source.gui.functions.Func import iter_layout

import threading

class AlbumExplorer(QtWidgets.QScrollArea):
    show_album = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)
        album_explorer_window = QtWidgets.QWidget()
        album_explorer_window.setLayout(QtWidgets.QVBoxLayout(self))
        self.albums = {}
        self.selected_album = None
        self.setWidget(album_explorer_window)
        self.setWidgetResizable(True)
        self.show_preview_thread = None

    # вынести в отдельный
    def reset_albums(self, names):
        self.selected_album = None
        layout = self.widget().layout()
        # clear
        list(map(lambda item: layout.removeItem(item), iter_layout(layout)))
        list(map(lambda album: album.deleteLater(), self.albums.values()))
        self.albums = {}
        # set
        albums = [AlbumWidget(name, self) for name in names]
        list(map(layout.addWidget, albums))
        self.albums.update(zip(map(lambda w: w.album_name, albums), albums))
        print(self.albums)
        list(map(lambda w: w.clicked.connect(self.select_album), 
                 self.albums.values()))
        self.select_album()

        self.stop_threads()
        self.show_preview_thread = threading.Thread(target=self.show_albums_preview)
        self.show_preview_thread.start()

    def show_albums_preview(self):
        for album in self.albums.values():
            album.show_preview()

    @QtCore.pyqtSlot(str)
    def select_album(self, name=None):
        self.set_checked(name)
        if self.selected_album is not None:
            self.show_album.emit(self.selected_album.album_name)

    @QtCore.pyqtSlot(str)
    def set_checked(self, name):
        if self.selected_album is not None:
            self.selected_album.set_is_clicked(False)
            self.selected_album.leaveEvent(None)
        values = self.albums.values()
        if name is not None:
            values = [self.albums.get(name)]
        for album_widget in filter(lambda v: v is not None, values):
            old_state = album_widget.blockSignals(True)
            album_widget.mouseDoubleClickEvent(None)
            album_widget.blockSignals(old_state)
            self.selected_album = album_widget
            break

    @QtCore.pyqtSlot()
    def stop_threads(self):
        if self.show_preview_thread:
            self.show_preview_thread.join()
            self.show_preview_thread = None