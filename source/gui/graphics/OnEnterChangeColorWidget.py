# -*- coding: utf-8 -*-
from PyQt5 import QtCore, QtGui, QtWidgets
from source.gui.albums.Album import Album
import sys


class OnEnterChangeColorWidget(QtWidgets.QWidget):
    clicked = QtCore.pyqtSignal(Album)

    def __init__(self, parent):
        super().__init__(parent)
        self.is_clicked = False
        self.setAutoFillBackground(True)
        self.parent = parent
    
    def set_color(self, color: QtGui.QColor, is_permanent=False):
        pal = self.palette()
        pal.setColor(QtGui.QPalette.Active, QtGui.QPalette.Window, color)
        if is_permanent:
            pal.setColor(QtGui.QPalette.Disabled, QtGui.QPalette.Window, color)
            pal.setColor(QtGui.QPalette.Inactive, QtGui.QPalette.Window, color)
        self.setPalette(pal)
    
    def set_permanent_color(self): 
        self.set_color(QtGui.QColor("#a2efed"), True)
    
    def set_active_color(self):   
        self.set_color(QtGui.QColor("#dbfcfb"))
        
    def set_nonactive_color(self):   
        pal = self.parent.palette()
        self.setPalette(pal)
        
    def enterEvent(self, event):
        if not self.is_clicked: 
            self.set_active_color()
        super().enterEvent(event)
            
    def leaveEvent(self, event):
        if not self.is_clicked:  
            self.set_nonactive_color() 
        super().leaveEvent(event)

    def set_is_clicked(self, clicked=True):
        self.is_clicked = clicked
