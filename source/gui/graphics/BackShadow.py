from PyQt5 import QtWidgets, QtGui

class BackShadow(QtWidgets.QGraphicsDropShadowEffect):
    """
    Прописывает графику для ScrollArea.
    """
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setBlurRadius(5)
        self.setXOffset(5)
        self.setYOffset(5)
        # gray color
        self.setColor(QtGui.QColor("#D3D3D3"))