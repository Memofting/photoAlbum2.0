from PyQt5 import QtWidgets, QtCore
from source.database.DataBase import DataBase

class AddDatabaseDialog(QtCore.QObject):
    """
    Нет проверки на то, является ли файл действительно базой данных. 
    Это остается на совести пользователя. 
    """
    success = QtCore.pyqtSignal(str)
    failed = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent

    def exec_(self):
        name, filt = QtWidgets.QFileDialog.getOpenFileName(
            parent=self.parent, caption="Add database", directory='~')
        if name:
            self.success.emit(name)
