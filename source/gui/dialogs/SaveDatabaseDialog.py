from PyQt5 import QtWidgets, QtCore
from source.database.DataBase import DataBase

class SaveDatabaseDialog(QtCore.QObject):
    success = QtCore.pyqtSignal(str)
    failed = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent

    def exec_(self):
        name, filt = QtWidgets.QFileDialog.getSaveFileName(
            parent=self.parent, caption="Save database", 
            directory='~', filter="All (*)")
        if name:
            self.success.emit(name)