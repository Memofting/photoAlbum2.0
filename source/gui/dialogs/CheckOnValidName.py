import re
from PyQt5 import QtWidgets, QtCore

class CheckOnValidName(QtCore.QObject):
    success = QtCore.pyqtSignal(str)
    failed = QtCore.pyqtSignal(str)
    
    valid_name = re.compile(
            r"^([\.][\w\d]+)|([\w\d_][\w\d\.\_ ]*)[\w\d\.\_]*$")

    def __init__(self, parent, *dialog_args, **dialog_kwargs):
        super().__init__(parent)
        self.parent = parent
        self.dialog_args = dialog_args
        self.dialog_kwargs = dialog_kwargs

    def exec_(self):
        s, ok = QtWidgets.QInputDialog.getText(self.parent, 
                                               *self.dialog_args, 
                                               **self.dialog_kwargs)
        if ok:
            if self.valid_name.match(s) is not None:
                self.success.emit(s)
            else:
                self.failed.emit(s)
