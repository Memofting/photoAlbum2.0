#!/usr/bin/env python3
import cv2
import os
from itertools import chain, starmap
from functools import partial
from PyQt5 import QtGui


IMAGE_PATH = os.path.join(".", "images") + "{}"
IMAGE_NAMES = [IMAGE_PATH.format(i) for i in [
    'bat1.jpg', 'bike.jpg', 'moustache.jpg', 'pink.jpg', 'silver1.jpg'
]]
EXTENTIONS = [
    "." + image_format.data().decode('utf-8')
    for image_format in QtGui.QImageReader.supportedImageFormats()]


def iter_filenames(path):
    """Возвращает итератор по всем файлам в дирректории и поддиректориях"""
    # сравнивай c помощью os.path.split
    return chain.from_iterable(
        starmap(
            lambda dp, _, fn: map(
                partial(os.path.join, os.path.normpath(dp)),
                map(os.path.normpath, fn)),
            os.walk(path))
    )


def with_extensions(extensions, filenames):
    """Фильтрует файлы по расширению"""
    return filter(lambda f: get_extension(f) in extensions, filenames)


def get_extension(filename):
    """Получает расширение имени файла"""
    return os.path.splitext(filename)[1]


def get_files_from(path: str, extensions: list = EXTENTIONS):
    """Возвращает итератор по именам файлов с данными
     расширениями из данной дирректории"""
    return map(
        os.path.normpath, with_extensions(
            extensions, iter_filenames(path)))
