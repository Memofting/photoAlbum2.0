from PyQt5 import QtGui, QtCore
from source.database.ImageInfo import ImageInfo

import itertools
import os
import numpy as np
import cv2 as cv

class AlbumInfo:
    """
    Contain album's description.
    """
    ICON_PATH = os.path.join(".", "images", "icons", "album.png")
    TEMP_PATH = os.path.join(".", "temp", "capture.jpg")
    
    def __init__(self, database, name: str, 
                 order: list=None, images: dict=None):
        self.database = database
        self.name = name
        self.collage_images = 1
        self.order = []
        self._order = []
        if order:
            self.order = order
            self._order = zip(range(-len(order), 0, 1), order)
        self.images = {}
        if images:
            self.images = images

    def get_preview(self):
        """
        Build preview image for album with images in album.
        """
        step = 10
        w, h = 124, 124

        image_names = list(itertools.islice(self.order, 2))
        print("ALbum: {}\nImage names: {}\n".format(self.name, image_names))
        amount = len(image_names)
        if not amount:
            return QtGui.QPixmap(self.ICON_PATH)

        left_corners = zip(*itertools.tee(range(0, amount * step, step)))
        back = 255 * np.ones((w+(amount - 1)*step, h+(amount-1)*step, 3))
        
        for image_name, corner in zip(image_names, left_corners):
            x, y = corner
            image = cv.imread(image_name, cv.IMREAD_COLOR)
            image = cv.resize(image, (w, h), interpolation=cv.INTER_LINEAR)
            iw, ih = image.shape[:2]
            back[x:x+iw, y:y+ih] = image
        cv.imwrite(self.TEMP_PATH, back)
        return QtGui.QPixmap(self.TEMP_PATH)

    def add_image(self, image_info: ImageInfo):
        cur_time = image_info.albums_names[self.name]
        self._order.append((cur_time, image_info.name))
        self.images.update({image_info.name: image_info})

    def sort_images(self):
        # все работает и так, но почему не надо сортировать order?
        # проблема возникает, только при одновременной загрузке из альбома 
        # и добавлении туда картинки, так как при этом обновляется время, а по нему мы и сортируем.
        self.order = [t[1] for t in sorted(self._order, key=lambda t: t[0])]

    def __eq__(self, other):
        if not isinstance(other, AlbumInfo):
            return False
        if (not self.name == other.name or
            not self.order == other.order or
            not self.images == other.images):
           return False
        return True 
        
