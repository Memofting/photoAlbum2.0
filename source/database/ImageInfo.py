import shelve
import os
import time
from source.database.ImageMetaData import ImageMetaData
from threading import Lock

class ImageInfo:
    """
    Интерфейс, для получения детальной информации из картинки.
    """
    def __init__(self, name, albums_names=None, image_bytes=None, ext="JPG"):
        """
        albums_names: list of str
        """
        self.name = name
        self.image_bytes = image_bytes
        self.albums_names = {}
        self.clusters = set()
        self.ext = ext
        self.meta_data = ImageMetaData(name)
        if albums_names is not None:
            for album in albums_names:
                self.add_albums_names(album=album)

    def merge(self, other_image_info: 'ImageInfo'):
        """
        Merge information about albums and clusters.
        Image_bytes and ext also will change. Information
        that will be in result is information with more
        recent time of changing.
        """
        self.image_bytes = other_image_info.image_bytes
        self.ext = other_image_info.ext
        self.update_albums_names(other_image_info.albums_names)

    def update_albums_names(self, new_albums_names):
        """
        Обновляет список альбомов в соответствии со временем добавления в них.
        """
        for album_name, new_time in new_albums_names.items():
            cur_time = self.albums_names.get(album_name, -1)
            self.albums_names.update({album_name: max(cur_time, new_time)})

    def total_update_image_info(self, **kvargs):
        for name in list(filter(lambda n: not n.startswith('__'), dir(self))):
            if kvargs.get(name, None) is not None:
                setattr(self, name, kvargs[name])

    def add_albums_names(self, album, cur_time=time.time()):
        """
        Добавляет картинки в альбом с учетом переданного времени, если
        время не было передано, то берется текущее время.
        """
        self.albums_names.update({album: cur_time})

    def remove_albums_names(self, albums_name: str):
        self.albums_names.pop(albums_name, None)

    def get_geo_tags(self):
        return self.meta_data.data.get(ImageMetaData.GPS_INFO_NAME, [None, None])

    def __eq__(self, other_image_info):
        """
        Simplified version of comparing two ImageInfo for testing requires.
        """
        if not isinstance(other_image_info, ImageInfo):
            return False
        elif self.image_bytes != other_image_info.image_bytes:
            return False
        elif self.ext != other_image_info.ext:
            return False
        cur_albums = sorted(self.albums_names.keys())
        other_albums = sorted(other_image_info.albums_names.keys())
        if cur_albums != other_albums:
            return False
        return True