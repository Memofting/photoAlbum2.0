# -*- coding: utf-8 -*-
import shelve
import os
import time
from itertools import islice
from threading import Lock
from source.database.ImageInfo import ImageInfo
from source.database.AlbumInfo import AlbumInfo

class DataBase:
    """
    Интерфейс для работы с базами данных, которые представляют альбомы.
    """
    _BASE_DIR = os.path.abspath(os.path.join('temp', 'album'))
    ORDER_NAME = "order"
    IMAGES_NAME = "images"

    def __init__(self, name):
        """
        name - путь до базы данных. Так как используется
        shelve, то в пути убирается расширение. Если это новая база,
        то name содержит имя для нее.
        """
        self.locker = Lock()
        if isinstance(name, DataBase):
            self.full_name = name.full_name
        else:
            os.makedirs(self._BASE_DIR, exist_ok=True)
            self.full_name = os.path.join(self._BASE_DIR, 
                                     os.path.splitext(name)[0])
            # if not self.create_if_correct(self.full_name):
            #     raise ValueError("Incorrect filename: {}".format(name))

    @staticmethod
    def get_base_dir():
        return DataBase._BASE_DIR

    def create_if_correct(self, name):
        """
        Возвращает результат создания файла с базой данных
        res: bool
        """
        try:
            with self.locker, shelve.open(name, 'c'):
                pass
        except Exception as exp:
            print(exp)
            return False
        else:
            return True

    def get_albums_info(self):
        """
        Получает информацию со всех альбомов.
        db: dict({
            image_name: ImageInfo
        })
        result: dict({
                    albums_name: dict({
                        order: list(images_names),
                        images: dict({image_name: ImageInfo})
                    })
                })
        """
        albums = dict()
        with self.locker:
            while True:
                try:
                    # избавляемся от ошибки _qdb: resource temporarily unawailable
                    with shelve.open(self.full_name, 'c') as db:
                        for image_info in db.values():
                            albums_names_info = image_info.albums_names
                            for album_name in albums_names_info.keys():
                                albums.setdefault(album_name, AlbumInfo(database=self,
                                                                        name=album_name))
                                albums[album_name].add_image(image_info)
                    break
                except Exception:
                    continue
        for album_info in albums.values():
            album_info.sort_images()
        return albums

    def add_image(self, image_name: str,
                  albums_name: str,
                  image_bytes: bytes=None,
                  ext: str=None):
        """
        Добавляет картинку в альбом.
        """
        with self.locker, shelve.open(self.full_name, 'c') as db:
            image_info = db.get(image_name, ImageInfo(name=image_name))
            image_info.add_albums_names(
                album=albums_name, cur_time=time.time())
            image_info.total_update_image_info(
                image_bytes=image_bytes, ext=ext)
            db.update({image_name: image_info})

    def get_image_info_by_name(self, image_name):
        with self.locker, shelve.open(self.full_name, 'c') as db:
            return db.get(image_name, ImageInfo(name=image_name))

    def remove_image(self, image_name: str, albums_name=None):
        """
        Удаляет картинку из альбома. Если альбом не указан, то удаляет саму
        запись о картинке из альбома.
        """
        with self.locker, shelve.open(self.full_name, 'c') as db:
            if albums_name is not None:
                image_info = db.get(image_name, ImageInfo(name=image_name))
                image_info.remove_albums_names(albums_name)
                db.update({image_name: image_info})
            else:
                db.pop(image_name, None)

    def merge(self, other_albums_info):
        """
        Сливает информацию с двух баз данных в одну. (текущую).
        В случае информации об одинаковых файлах, старая информация
        заменяется на новую.
        """
        with self.locker, shelve.open(self.full_name, 'c') as db:
            other_db = DataBase(other_albums_info)
            other_albums_info = other_db.get_albums_info()
            for album_info in other_albums_info.values():
                for image_name in album_info.order:
                    image_info = db.get(image_name, ImageInfo(name=image_name))
                    image_info.merge(album_info.images[image_name])
                    db.update({image_name: image_info})

    def clear(self, albums_name: str = None):
        """
        Чистит содержимое базы дынных. Если указано имя альбома,
        то чистит информацию, связанную только с этим альбомом.
        """
        with self.locker, shelve.open(self.full_name, 'c') as db:
            if albums_name is not None:
                for image_name, image_info in db.items():
                    image_info.remove_albums_names(albums_name=albums_name)
                    if not image_info.albums_names:
                        db.pop(image_name, None)
                    else:
                        db.update({image_name: image_info})
            else:
                db.clear()

    def save(self, new_path: str):
        """
        Сохраняет содержимое базы данных в файл, с переданным именем.
        :param new_path: файл с новым именем базы данных
        :return: None
        """
        with self.locker, shelve.open(self.full_name, 'c') as db:
            with shelve.open(new_path, 'c') as other_db:
                for image, info in db.items():
                    other_db.update({image: info})
