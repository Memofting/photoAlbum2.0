from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS
import itertools
import os
import struct


class ImageMetaData():
    LONGITUDE_NAME = "Longitude"
    LATITUDE_NAME = "Latitude"
    GPS_INFO_NAME = "GPSInfo"

    def __init__(self, full_image_name):
        # так как мы будем серилизовать данный объект, то 
        # хранить подобные картинки не выгодно
        self.format = ""
        self.header = {}
        self.data = {}

        if os.path.exists(full_image_name):
            image = Image.open(full_image_name)
            self.format = image.format
            self.header = [
                ("Name", full_image_name),
                ("Type", image.format)]
            if self.format == "JPEG":
                self.data = self.get_exif_data(image._getexif())

    def __str__(self):
        return "\n".join(
            ("{}: {}".format(k, v) for k,v in itertools.chain(
                 self.header, self.data.items()))) 

    def get_exif_data(self, info):
        """
        Returns a dictionary from the exif data of an PIL Image item. 
        Also converts the GPS Tags
        """
        exif_data = {}
        if self.format != "JPEG":
            return exif_data
        if info:
            for tag, value in info.items():
                decoded = TAGS.get(tag, tag)
                if decoded == ImageMetaData.GPS_INFO_NAME:
                    gps_data = {}
                    for t in value:
                        sub_decoded = GPSTAGS.get(t, t)
                        gps_data[sub_decoded] = value[t]

                    exif_data[decoded] = ImageMetaData.get_lat_lng(gps_data)
                else:
                    exif_data[decoded] = value
                    try:
                        if isinstance(value, bytes):
                            exif_data[decoded] = int(value, 2)
                    except ValueError:
                        pass
        return exif_data

    @staticmethod
    def convert_to_degress(mesuare_val):

        """
        Convert GPS coordinates stored in the
        EXIF to degress in float format
        """
        d0 = mesuare_val[0][0]
        d1 = mesuare_val[0][1]
        d = float(d0) / float(d1)

        m0 = mesuare_val[1][0]
        m1 = mesuare_val[1][1]
        m = float(m0) / float(m1)

        s0 = mesuare_val[2][0]
        s1 = mesuare_val[2][1]
        s = float(s0) / float(s1)

        return d + (m / 60.0) + (s / 3600.0)

    @staticmethod
    def get_lat_lng(gps_info: dict):        
        """
        Returns the latitude and longitude, if available,
        from the provided exif_data (obtained through get_exif_data above)
        """
        lat = None
        lng = None  
        gps_latitude = gps_info.get("GPSLatitude", None)
        gps_latitude_ref = gps_info.get('GPSLatitudeRef', None)
        gps_longitude = gps_info.get('GPSLongitude', None)
        gps_longitude_ref = gps_info.get('GPSLongitudeRef', None)
        if gps_latitude and gps_latitude_ref and gps_longitude and gps_longitude_ref:
            lat = ImageMetaData.convert_to_degress(gps_latitude)
            if gps_latitude_ref != "N":                     
                lat = 0 - lat
            lng = ImageMetaData.convert_to_degress(gps_longitude)
            if gps_longitude_ref != "E":
                lng = 0 - lng
        return [lat, lng]