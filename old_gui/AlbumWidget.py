import os
import sys
from PyQt5 import QtWidgets, QtCore, QtGui
from source.gui.Album import Album
from source.database.DataBase import DataBase
from source.gui.OnEnterChangeColorWidget import OnEnterChangeColorWidget


class AlbumWidget(OnEnterChangeColorWidget):
    """
    Класс, отражающий функциональный элемент альбома на главном
    окне.
    """
    ICON_PATH = os.path.join(".", "images", "icons", "album.png")
    STAND_BACK_COLOR = "#ffffff"
    clicked = QtCore.pyqtSignal(Album)

    def __init__(self, album_name, parent):
        super().__init__(parent)
        if isinstance(album_name, Album):
            self.album = album_name
        elif isinstance(album_name, str):
            self.album = Album(album_name, DataBase("LocalDatabase"))
        self.album_name = self.album.name
        self.image = QtWidgets.QLabel()
        self.image.setPixmap(QtGui.QPixmap(self.ICON_PATH))

        self.name = QtWidgets.QLabel(self.album_name)
        self.name.setAlignment(QtCore.Qt.AlignCenter)

        self.vbox = QtWidgets.QVBoxLayout()
        self.vbox.addWidget(self.name)
        self.vbox.addWidget(self.image)
        self.vbox.addStretch(1)

        self.setLayout(self.vbox)
        self.set_nonactive_color()
        self.resize(300, 300)
    
    def mouseDoubleClickEvent(self, a0: QtGui.QMouseEvent):
        self.set_permanent_color()
        self.set_is_clicked()
        self.clicked.emit(self.album)


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    window = QtWidgets.QWidget()
    vbox = QtWidgets.QVBoxLayout()
    for i in range(5):
        vbox.addWidget(AlbumWidget(str(i), window))
    window.setLayout(vbox)
    window.show()
    sys.exit(app.exec_())
