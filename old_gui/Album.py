#!usr/bin/env python3
from PyQt5 import QtCore, QtGui
from source.database.DataBase import DataBase

EXTENSION = {
    image_format.data().decode('utf-8').upper()
    for image_format in QtGui.QImageReader.supportedImageFormats()}


class Album:
    """
    Класс который отвечает сущности альбома. В инициализаторе он принимает
    свое уникальное имя, и имя родительской базы данных.
    """
    def __init__(self, name, parent_database: DataBase):
        """
        При инициализации мы можем либо загрузить уже имеющийся альбом, либо
        создать новый.
        """
        self.name = name
        self.parent_database = parent_database

    def get_info(self):
        whole_db_data = self.parent_database.get_albums_info().get(
            self.name, {
                DataBase.ORDER_NAME: list(),
                DataBase.IMAGES_NAME: dict()})
        return {
            DataBase.ORDER_NAME: whole_db_data[DataBase.ORDER_NAME],
            DataBase.IMAGES_NAME: whole_db_data[DataBase.IMAGES_NAME]
        }

    @QtCore.pyqtSlot(str)
    def add_image(self, full_image_name, image_bytes: bytes, ext="JPG"):
        self.parent_database.add_image(
            image_name=full_image_name,
            albums_name=self.name,
            image_bytes=image_bytes,
            ext=ext
        )

    @QtCore.pyqtSlot(str)
    def remove_image(self, full_image_name):
        self.parent_database.remove_image(
            image_name=full_image_name,
            albums_name=self.name)

    @QtCore.pyqtSlot()
    def clear(self):
        self.parent_database.clear(albums_name=self.name)

    @QtCore.pyqtSlot()
    def merge(self, other_albums_db: DataBase):
        self.parent_database.merge(other_albums_db)

    @QtCore.pyqtSlot()
    def delete(self):
        self.clear()
        pass
