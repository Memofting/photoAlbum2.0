#!/usr/bin/env python3
from PyQt5 import QtCore, QtWidgets, QtGui
import os
import threading
import itertools
import functools
from sklearn.cluster import KMeans
import numpy as np

from source.database.ImageMetaData import ImageMetaData
from source.database.DataBase import DataBase
import source.database.ImageGetter as imWin

from source.gui.ScrollLabel import MyLabel
from source.gui.Album import Album, EXTENSION

class ThreadWithStopSlot(QtCore.QObject):
    def __init__(self):
        super().__init__()
        self.is_stopped = False

    @QtCore.pyqtSlot()
    def stop(self):
        self.is_stopped = True
    

class SaveImageThread(threading.Thread, QtCore.QObject):
    def __init__(self, album, image_name, image):
        super().__init__()
        self.album = album
        self.image_name = image_name
        self.image = image

    @staticmethod
    def get_image_bytes_match_extension(image_name: str, image: QtGui.QImage):
        pixmap = QtGui.QPixmap.fromImage(image)
        byte_buf = QtCore.QByteArray()
        buf = QtCore.QBuffer(byte_buf)
        buf.open(QtCore.QIODevice.WriteOnly)
        ext = os.path.splitext(image_name)[1][1:].upper()
        if ext in EXTENSION:
            pixmap.save(buf, ext)
            buf.close()
            return byte_buf.data(), ext
        return b'', ''

    def run(self):
        bytes_and_ext = self.get_image_bytes_match_extension(
            self.image_name, self.image
        )
        self.album.add_image(self.image_name, *bytes_and_ext)


class UploadTread(QtCore.QObject, threading.Thread):
    """
    Тред для обновления базы данных
    """
    def __init__(self, scroll_area, parent=None):
        super().__init__(parent)
        self.scroll_area = scroll_area      
        self.is_stopped = False

    @QtCore.pyqtSlot()
    def stop(self):
        self.is_stopped = True
        

    @staticmethod
    def get_image_bytes_match_extension(image_name: str, image: QtGui.QImage):
        pixmap = QtGui.QPixmap.fromImage(image)
        byte_buf = QtCore.QByteArray()
        buf = QtCore.QBuffer(byte_buf)
        buf.open(QtCore.QIODevice.WriteOnly)
        ext = os.path.splitext(image_name)[1][1:].upper()
        if ext in EXTENSION:
            pixmap.save(buf, ext)
            buf.close()
            return byte_buf.data(), ext
        return b'', ''

    def run(self):
        """
        Инициализирует загрузку картинок в базу данных
        """
        all_image_names = filter(
            lambda img: img not in self.scroll_area.showed_images,
            self.scroll_area.image_names)
        for _ in range(self.scroll_area.upload_lines):
            image_names = itertools.islice(
                all_image_names,
                self.scroll_area.elements_on_line)
            """
             Загружаем картинки в базу данных
            """
            for image_name, image in (
                self.scroll_area.get_scaled_images_with_names(
                    image_names, self.scroll_area.w, self.scroll_area.h)):
                    if self.is_stopped:
                        return
                    bytes_and_ext = self.get_image_bytes_match_extension(
                        image_name, image
                    )
                    self.scroll_area.album.add_image(
                        image_name, *bytes_and_ext)


class UploadAlbumTread(QtCore.QObject, threading.Thread):
    """
    Тред для загрузки картинок из базы данных
    """

    start_upload = QtCore.pyqtSignal(Album, str)
    uploaded = QtCore.pyqtSignal(Album, str, QtGui.QImage)

    def __init__(self, scroll_area, parent=None):
        super().__init__(parent)
        self.scroll_area = scroll_area        
        self.is_stopped = False

    @QtCore.pyqtSlot()
    def stop(self):
        self.is_stopped = True
    

    def run(self):
        """
        Возвращает scroll_area.elements_on_line картинок, загруженных с диска
        """
        info = self.scroll_area.album.get_info()
        all_image_names = filter(
            lambda img: img not in self.scroll_area.showed_images,
            info[DataBase.ORDER_NAME])
        for _ in range(self.scroll_area.upload_lines):
            image_names = itertools.islice(
                all_image_names,
                self.scroll_area.elements_on_line)
            for image_name in image_names:
                if self.is_stopped:
                    return
                self.start_upload.emit(self.scroll_area.album, image_name)
                image_info = info[DataBase.IMAGES_NAME][image_name]
                data = image_info.image_bytes
                ext = image_info.ext
                image_bytes = QtCore.QByteArray(data)
                image = QtGui.QImage()
                image.loadFromData(image_bytes, ext)
                self.uploaded.emit(self.scroll_area.album, image_name, image)
                self.scroll_area.showed_images.add(image_name)


class MyScrollArea(QtWidgets.QScrollArea):
    """Реализует QScrollArea с QGridLayout в центре, который подгружает в себя
       картинки, по достижении колесиком мыши нижнего положение QScrollBar. 
       Для Этого используется тред - UploadTread, который по мере загрузки
       изображений производит сигнал uploaded.

       поле self.image_names - содержит естественный порядок, в котором
       изображения будут подгружаться в scroll_area
       """
    IMAGE_ICON = os.path.join(".", "images", "icons", "image_icon.png")
    LOCAL_IMAGES_PATH = os.path.join("~", "")
    DEFAULT_ALBUM_NAME = "LocalAlbum"
    clicked_image = QtCore.pyqtSignal(str)
    create_new_with_album = QtCore.pyqtSignal(Album)

    def __init__(self, path="", img_size=(100, 100),
                 elements_on_line=1, start_lines=1, upload_lines=1,
                 album_name="LocalAlbum",
                 parent_database=DataBase("LocalDatabase"),
                 parent=None):
        super().__init__(parent)
        if isinstance(album_name, Album):
            self.album = album_name
        else:
            self.album = Album(
                name=album_name,
                parent_database=parent_database)
        self.parent_database = parent_database
        self.upload_album_thread = UploadAlbumTread(self)
        self.upload_album_thread.uploaded.connect(
            self.update_ui)
        self.upload_album_thread.start_upload.connect(
            self.prepare_place_for_image
        )
        self.upload_thread = UploadTread(self)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setWidgetResizable(True)

        self.w, self.h = img_size
        self.elements_on_line = elements_on_line
        self.start_lines = start_lines
        self.upload_lines = upload_lines

        self.layout = QtWidgets.QGridLayout()
        self.box = QtWidgets.QWidget()
        self.box.setLayout(self.layout)

        self.setWidget(self.box)
        self.scroll_bar = self.verticalScrollBar()
        self.cur_slider_position = 0
        self.last_slider_position = 0

        self.last_clicked_image = None
        self.showed_images = set()
        self.init_image_download_field(path)
        self.upload_thread.start()

    def init_image_download_field(self, path=""):
        self.row_num = 0
        self.col_num = 0
        self.upload_row_num = 0
        self.upload_col_num = 0

        self.image_names = imWin.get_files_from(imWin.EXTENTIONS, path)

        _, self.upload_image_icon = (
            next(self.get_scaled_images_with_names(
                [MyScrollArea.IMAGE_ICON], self.w, self.h)))

    @staticmethod
    def name_from_geo_tag(tag):
        return "GEOTAG: {:=6};{:=6}".format(*tag)

    def upload_kmeans_from_image_geo_tags(self, n_clucters: int=4):
        self.stop_uploading()
        if not self.showed_images:
            return
        images_info = list(
            self.parent_database.get_image_info_by_name(
                *list(self.showed_images)))
        geo_tags = np.array(
            list(filter(lambda ll: ll[0] != None and ll[1] != None, 
                        (image_info.get_geo_tags() 
                        for image_info in images_info))), 
            dtype='float64')
        kmeans = KMeans(n_clusters=n_clucters, random_state=0).fit(geo_tags)
        centers = kmeans.cluster_centers_
        
        for image_info in images_info:
            tags = image_info.get_geo_tags()
            if tags[0] != None and tags[1] != None:
                center = centers[kmeans.predict(np.array([tags]))[0]]
                # загрузка базы данных
                self.parent_database.add_image(
                    image_name=image_info.name,
                    image_bytes=image_info.image_bytes,
                    ext=image_info.ext, 
                    albums_name=self.name_from_geo_tag(center))


    def add_images(self, image_names):
        self.image_names = itertools.chain(self.image_names, image_names)

    def add_dir(self, directory):
        self.image_names = itertools.chain(
            self.image_names,
            imWin.get_files_from(imWin.EXTENTIONS, directory)
        )

    @QtCore.pyqtSlot(Album, str, QtGui.QImage)
    def update_ui(self, album, image_name, image):
        """Подгружает картинку в последнюю строчку в scroll_area"""
        if album == self.album:
            upload_item = (
                self.layout.itemAtPosition(self.row_num, self.col_num))
            if upload_item is not None:
                label = MyLabel(image_name=image_name,
                                image=image, w=self.w, h=self.h,
                                parent=self)
                label.clicked.connect(self.double_click_on_image)
                self.layout.replaceWidget(upload_item.widget(), label)
                upload_item.widget().deleteLater()
                self.col_num += 1
                if self.col_num == self.elements_on_line:
                    self.col_num = 0
                    self.row_num += 1
            else:
                error_message = (
                    "ERROR: can\'t find item in col {},row {}. "
                    "Item is {}".format(
                        self.col_num, self.row_num, upload_item))
                raise ValueError(error_message)

    def prepare_place_for_image(self, album, *args):
        if album == self.album:
            label = MyLabel(image=self.upload_image_icon,
                            w=self.w, h=self.h, parent=self)
            self.layout.addWidget(label,
                                  self.upload_row_num,
                                  self.upload_col_num,
                                  alignment=QtCore.Qt.AlignCenter)
            self.upload_col_num += 1
            if self.upload_col_num >= self.elements_on_line:
                self.upload_row_num += 1
                self.upload_col_num = 0

    def wheelEvent(self, event):
        """Переопределение метода прокрутки колесика мыши,
           с целью подгрузки фото"""
        self.cur_slider_position = self.scroll_bar.sliderPosition()
        if (self.cur_slider_position == self.last_slider_position ==
           self.scroll_bar.maximum() and event.angleDelta().y() < 0):
            self.upload_thread.start()
            self.upload_album_thread.start()

        self.last_slider_position = self.cur_slider_position
        event.ignore()
        QtWidgets.QScrollArea.wheelEvent(self, event)

    @staticmethod
    def get_scaled_images_with_names(image_names, w, h):
        for image_name in image_names:
            image = QtGui.QImage(image_name)
            if (image.size().width() > w or
               image.size().height() > h):
                aspect_ratio_mode = QtCore.Qt.KeepAspectRatio
                transform_mode = QtCore.Qt.SmoothTransformation
                yield (image_name,
                       image.scaled(w, h,
                                    aspectRatioMode=aspect_ratio_mode,
                                    transformMode=transform_mode))
            else:
                yield (image_name, image)

    @QtCore.pyqtSlot(MyLabel)
    def double_click_on_image(self, my_label):
        """
        Посылает сигнал с названием елемента label
        """
        if self.last_clicked_image:
            self.last_clicked_image.set_nonactive_color()
        self.last_clicked_image = my_label
        self.last_clicked_image.set_is_clicked()
        self.clicked_image.emit(my_label.name)

    @QtCore.pyqtSlot(Album)
    def upload_image_icons(self, album):
        """
        Действуем в предположении, что если альбом уже находится
        в scroll_area, то его обновлять не нужно.
        """
        self.album = album
        self.create_new_with_album.emit(self.album)

    def show_albums_entity(self):
        # Этот и следующие while жизненно необходимы
        # без них программа падает. Спроси почему.
        self.stop_uploading()
        self.upload_album_thread.start()

    def clear_albums_entity(self):
        self.album.clear()

    def stop_uploading(self):
        self.upload_album_thread.stop()
        if self.upload_thread.isAlive():
            self.upload_album_thread.join()
        self.upload_thread.stop()
        if self.upload_thread.isAlive():
            self.upload_thread.join()
