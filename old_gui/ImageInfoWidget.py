#!/usr/bin/env python3
from PyQt5 import QtCore, QtWidgets, QtGui
import sys
from source.database.ImageMetaData import ImageMetaData
from source.database.DataBase import ImageInfo


def empty_func(*args, **kargs):
    pass


class ScaledImage(QtWidgets.QLabel):
    MIN_SIZE_PRODUCT = 10e2
    MAX_SIZE_PRODUCT = 10e6

    def __init__(self, image_name, parent=None):
        super().__init__(parent)

        if isinstance(image_name, str):
            self.original_image = QtGui.QImage(image_name)
        elif isinstance(image_name, QtGui.QImage):
            self.original_image = image_name

        if not self.original_image.isNull():
            self.aspect_ratio = (
                    self.original_image.width() /
                    self.original_image.height())
            self.setFrameStyle(QtWidgets.QFrame.Box |
                               QtWidgets.QFrame.Panel)
            self.setAlignment(QtCore.Qt.AlignCenter)
            self.setPixmap(QtGui.QPixmap.fromImage(self.original_image))
            self.setSizePolicy(
                QtWidgets.QSizePolicy.Minimum,
                QtWidgets.QSizePolicy.Minimum
            )

    def wheelEvent(self, a0: QtGui.QWheelEvent):
        image = self.pixmap().toImage()
        delta = a0.angleDelta().y() // 3
        width = image.width() + delta
        height = width / self.aspect_ratio
        if (self.MIN_SIZE_PRODUCT < width*height <
           self.MAX_SIZE_PRODUCT and width + height > 0):
            image = QtGui.QImage(
                self.original_image.scaled(
                    width, round(height),
                    transformMode=QtCore.Qt.SmoothTransformation))
            self.setPixmap(QtGui.QPixmap.fromImage(image))


class ImageDescription(QtWidgets.QWidget):
    def __init__(self, image_name, parent=None):
        super().__init__(parent)
        self.description = QtWidgets.QLabel()
        self.description.setFrameStyle(QtWidgets.QFrame.Box |
                                       QtWidgets.QFrame.Panel)
        self.description.setAlignment(QtCore.Qt.AlignLeft |
                                      QtCore.Qt.AlignTop)
        self.description.setWordWrap(True)
        
        if isinstance(image_name, ImageInfo):
            self.meta_data = image_name.meta_data
        else:
            self.meta_data = ImageMetaData(image_name)
        
        self.description.setText(str(self.meta_data))

        self.hbox = QtWidgets.QHBoxLayout()
        self.hbox.addWidget(self.description, 1)

        self.setLayout(self.hbox)
        


class ImageInfoWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.description = None
        self.image_widget = None

        self.scroll_area_for_image = QtWidgets.QScrollArea(self)
        self.scroll_area_for_image.setVerticalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOn
        )
        self.scroll_area_for_image.setHorizontalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOn
        )
        self.scroll_area_for_image.wheelEvent = empty_func
        self.scroll_area_for_image.setWidgetResizable(True)

        self.scroll_area_for_description = QtWidgets.QScrollArea(self)
        self.scroll_area_for_description.setWidgetResizable(True)

        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.main_layout.addWidget(self.scroll_area_for_image, stretch=2)
        self.main_layout.addWidget(self.scroll_area_for_description, stretch=1)

    def upload_image(self, image_name: str, image: QtGui.QImage):
        if self.image_widget or self.description:
            self.image_widget.deleteLater()
            self.description.deleteLater()
        self.image_widget = ScaledImage(image, self)
        for image_info in self.parent.database.get_image_info_by_name(image_name):
            break
        self.description = ImageDescription(image_info, self)
        self.scroll_area_for_image.setWidget(self.image_widget)
        self.scroll_area_for_description.setWidget(self.description)

